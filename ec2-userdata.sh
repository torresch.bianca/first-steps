#!/bin/bash
apt-get update -y && apt-get upgrade -y
apt install nodejs -y
apt install npm -y
mkdir /home/ubuntu/node-app
chown -R ubuntu:ubuntu /home/ubuntu/node-app